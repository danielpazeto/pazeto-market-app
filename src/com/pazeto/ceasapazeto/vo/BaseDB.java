package com.pazeto.ceasapazeto.vo;

import android.content.ContentValues;

public abstract class BaseDB {
	public abstract ContentValues getAsContentValue();

}
